$(document).ready(function(){
    var report,
        chartLabels = [],
        chartData = [];
    var options = {};

    // turns programs into an associative array
    var programsArray = [];
    programs.forEach(program => {
        programsArray[program.id] = program.name;
    });
    programs = programsArray;

    // turns categories into an associative array
    var categoriesArray = [];
    categories.forEach(category => {
        categoriesArray[category.id] = category.name;
    });
    categories = categoriesArray;

    // defines chart colors
    var bgColors = [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
    ];
    var borderColors = [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
    ];

    // updates the chart data
    function updateChart() {
        
        // menu item values
        var field = $('#field').val();
        var subField = $('#subField').val();
        var see = $('#see').val();

        // title components
        var fieldTitle = '',
            subFieldTitle = '';
        
        // chart data
        var labels;
        chartLabels = [];
        chartData = [];

        // creates content to populate chart
        applications.forEach(application => {
            var index = -1;
            var column;
            if(field == 'program') {
                
                // checks if the user selected a sub-field
                if(subField == 0) {
                    
                    // adds all program data
                    column = application.program;
                    labels = programs;
                    fieldTitle = 'all Programs';
                } else {
                    
                    // adds category data only if the application's program matches the one selected by the user
                    if(application.program == subField) {
                        column = application.category;
                        labels = categories;
                        subFieldTitle = programs[subField];
                    }
                }
            } else {

                // checks if the user selected a sub-field
                if(subField == 0) {
                    
                    // adds all category data
                    column = application.category;
                    labels = categories;
                    fieldTitle = 'all Categories';
                } else {
                    
                    // adds program data only if the application's category matches the one selected by the user
                    if(application.category == subField) {
                        column = application.program;
                        labels = programs;
                        subFieldTitle = categories[subField];
                    }
                }
            }
            if(column !== undefined) {

                // populates the label and data arrays
                index = chartLabels.findIndex(element => {
                    return element == column;
                });
                if(index == -1) {
                    chartLabels.push(column);
                    if(see == 'number') chartData[chartLabels.length - 1] = 1;
                    else chartData[chartLabels.length - 1] = parseFloat(((parseFloat(application.unit_price.replace('$','')) * parseInt(application.quantity)) + parseFloat(application.shipping.replace('$','')) + parseFloat(application.setup_charges.replace('$',''))).toFixed(2));
                } else {
                    if(see == 'number') chartData[index]++;
                    else chartData[index] += parseFloat(((parseFloat(application.unit_price.replace('$','')) * parseInt(application.quantity)) + parseFloat(application.shipping.replace('$','')) + parseFloat(application.setup_charges.replace('$',''))).toFixed(2));
                }
            }
        });
        options = {
            legend: {
                display: true,
                labels: {
                    generateLabels: function(chart) {
                        return chart.data.labels.map(function(label, i) {
                            var ds = chart.data.datasets[0];
                            var arc = chart.getDatasetMeta(0).data[i];
                            var arcOpts = chart.options.elements.arc;
                            var custom = arc && arc.custom || {};
                            var getValueAtIndexOrDefault = Chart.helpers.getValueAtIndexOrDefault;
                            return {
                                text: labels[label],
                                fillStyle: custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor),
                                strokeStyle: custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor),
                                lineWidth: custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth),
                                //hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
                                index: i
                            };
                        });
                    }
                }
            },
            title: {
                display: true,
                text: (see == 'number' ? 'Number of Purchases' : 'Cost of all Purchases') + ' for ' + (fieldTitle.length > 0 ? fieldTitle : subFieldTitle)
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var label = labels[data.labels[tooltipItem.index]] || '';
                        var label_data = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] || '';
                        if($('#see').val() == 'cost') label += ': $' + label_data.toFixed(2);
                        else label += ': ' + label_data;
                        return label;
                    }
                }
            }
        }
        // creates the chart object if one does not exist yet
        if(report == undefined) {
            report = new Chart($('#report'), {
                type: 'pie',
                data: {
                    labels: chartLabels,
                    datasets: [{
                        label: 'Report',
                        data: chartData,
                        backgroundColor: bgColors,
                        borderColor: borderColors,
                        borderWidth: 1
                    }]
                },
                options: options
            });
        } else {
            
            // remove all the data
            report.data.datasets.forEach((dataset) => {
                dataset.data.pop();
            });

            // add the new data
            report.data.labels = chartLabels;
            report.data.datasets.forEach((dataset) => {
                dataset.data = chartData;
            });

            // update the chart options
            report.options = options;
            report.update();
        }
    }

    // initializes the chart with the default data
    $('#field').change(() => {
        $('#subField').children().remove();
        $('#subField').append('<option selected="selected" value="0">None</option>');
        if($('#field').val() == 'program') {
            programs.forEach((program, index) => {
                $('#subField').append(`<option value="${index}">${program}</select>`);
            });
        } else {
            categories.forEach((category, index) => {
                $('#subField').append(`<option value="${index}">${category}</select>`);
            });
        }
    });
    $('.form-control').change(updateChart);
    updateChart();
});