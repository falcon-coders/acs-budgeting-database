// waits to start running script until the page has fully rendered
$(document).ready(function(){
	var unitCost = $('#unitCost').val(),
	unitQty = $('#unitQty').val(),
	setupFee = $('#setup_fee').val(),
	shipping = $('#shipping_fee').val(),
	subTotal,
	total;

	// updates the total cost element
	function calculateTotal() {
		if(typeof unitCost == 'undefined' || unitCost == '' || unitCost == 0)
			unitCost = 0;
		else
			unitCost = parseFloat($('#unitCost').val());
		if(typeof unitQty == 'undefined' || unitQty == '' || unitQty == 1)
			unitQty = 1;
		else
			unitQty = parseInt($('#unitQty').val());
		if(typeof setupFee == 'undefined' || setupFee == '' || setupFee == 0)
			setupFee = 0;
		else
			setupFee = parseFloat($('#setup_fee').val());
		if(typeof shipping == 'undefined' || shipping == '' || shipping == 0)
			shipping = 0;
		else
			shipping = parseFloat($('#shipping_fee').val());
		subTotal = unitCost * unitQty;
		total = subTotal + setupFee + shipping;
		$('#subTotalCost').val('$' + subTotal.toFixed(2));
		$('#totalCost').val('$' + total.toFixed(2));
	}

	// determines if the form is ready to be submitted
	function validateApplication() {
		var isValid = true;
		$('[required=true]').each(function(index) {
			if($(this).val() == null || $(this).val().length == 0 || ($(this).is('select') && $(this).val() == 'Select')) {
				isValid = false;
				$(this).addClass('invalid-input');
				$(this).parent().find('.invalid-feedback').show();
			} else {
				$(this).removeClass('invalid-input');
				$(this).parent().find('.invalid-feedback').hide();
			}
		});
		console.log(isValid);
		return isValid;
	}

	// determines if the draft can be saved or not
	function validateDraft() {
		var isValid = true;
		if($('#itemName').val().length == 0) {
			isValid = false;
			$('#itemName').addClass('invalid-input');
			$('#itemName').parent().find('.invalid-feedback').show();
		}
		return isValid;
	}

	// makes sure that the cost is always a float with exactly 2 decimal places
	$('#unitCost').change(function() {
		unitCost = parseFloat($('#unitCost').val());
		if(Number.isNaN(unitCost)){
			unitCost = 0;
		}
		unitCost = parseFloat(unitCost).toFixed(2);
		$('#unitCost').val(unitCost);
		calculateTotal();
	});

	// makes sure that the quantity is always an integer
	$('#unitQty').change(function() {
		unitQty = Math.floor($('#unitQty').val());
		if(Number.isNaN(unitQty)){
			unitQty = 1;
		}
		$('#unitQty').val(unitQty);
		calculateTotal();
	});

	// makes sure that the setup fee is always a float with exactly 2 decimal places
	$('#setup_fee').change(function() {
		setupFee = parseFloat($('#setup_fee').val());
		if(Number.isNaN(setupFee)){
			setupFee = 0;
		}
		setupFee = parseFloat(setupFee).toFixed(2);
		$('#setup_fee').val(setupFee);
		calculateTotal();
	});

	// makes sure that the cost is always a float with exactly 2 decimal places
	$('#shipping_fee').change(function() {
		shipping = parseFloat($('#shipping_fee').val());
		if(Number.isNaN(shipping)){
			shipping = 0;
		}
		shipping = parseFloat(shipping).toFixed(2);
		$('#shipping_fee').val(shipping);
		calculateTotal();
	});

	// changes the value of the hidden action field before submission based on which button was clicked
	$('#save_button').click(function() {
		$('#action').val('save');
		$('#application').submit();
	});
	$('#submit_button').click(function() {
		$('#action').val('submit');
		$('#application').submit();
	});

	// listener for form submission
	$('#application').submit(function(event) {
		if($('#action').val() !== 'save') {
			if(!validateApplication()) {
				event.preventDefault();
				$('#error_container').html('<div class="error_message">You must fix all errors before continuing.</div>');
			}
		} else {
			if(!validateDraft()) {
				event.preventDefault();
				$('#error_container').html('<div class="error_message">You must fix all errors before continuing.</div>');
			}
		}
	});
	calculateTotal();
});