var programHTML = `
<div class="row">
	<div class="col-sm-10">
		<input class="form-control program-name" name="programCreateNames" type="text" />
	</div>
	<div class="col-sm-2">
		<button class="btn btn-block btn-dark delete" type="button">Delete</button>
	</div>
</div>`;

var categoryHTML = `
<div class="row">
	<div class="col-sm-10">
		<input class="form-control category-name" name="categoryCreateNames" type="text" />
	</div>
	<div class="col-sm-2">
		<button class="btn btn-block btn-dark delete" type="button">Delete</button>
	</div>
</div>`;

// waits to start running script until the page has fully rendered
$(document).ready(() => {
	var deletedPrograms = [],
		deletedCategories = [];

	/*window.addEventListener("hashchange", function(e){
		console.log('poop');
		if(window.confirm("Do you really want to leave? Changes will not be saved.")) {
			
			e.returnValue = '';
		}
		e.preventDefault();
	});*/

	$('#cancel').click(function(e) {
		location.reload(true);
	});

	// deletes a program field
	function deleteProgram() {
		var index = $(this).parent().parent().find('input').attr('data-program-id');
		deletedPrograms.push(index);
		$(this).closest('.row').remove();
	}
	$('#programFields').on('click', '.delete', deleteProgram);

	// deletes a category field
	function deleteCategory() {
		var index = $(this).parent().parent().find('input').attr('data-category-id');
		deletedCategories.push(index);
		$(this).closest('.row').remove();
	}
	$('#categoryFields').on('click', '.delete', deleteCategory);

	// adds a new program field
	$('#addProgram').click(() => {
		$('#programFields').append(programHTML);
	});

	// adds a new category field
	$('#addCategory').click(() => {
		$('#categoryFields').append(categoryHTML);
	});

	// creates the function that runs whenever the user attempts to submit the form
	$('#settings').submit((event) => {
		$('input[data-program-id]').each(function(index) {
			$('#settings').append(`<input name="programUpdateIDs" type="hidden" value="${$(this).attr('data-program-id')}" />`);
		});
		$('input[data-category-id]').each(function(index) {
			$('#settings').append(`<input name="categoryUpdateIDs" type="hidden" value="${$(this).attr('data-category-id')}" />`);
		});
		deletedPrograms.forEach((deletedProgram) => {
			if(typeof deletedProgram !== 'undefined') {
				$('#settings').append(`<input name="programDeletedIDs" type="hidden" value="${deletedProgram}" />`);
			}
		});
		deletedCategories.forEach((deletedCategory) => {
			if(typeof deletedCategory !== 'undefined') {
				$('#settings').append(`<input name="categoryDeletedIDs" type="hidden" value="${deletedCategory}" />`);
			}
		});
	});
});