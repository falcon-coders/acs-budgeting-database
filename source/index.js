const compression = require('compression');
var bodyParser = require('body-parser');
const express = require('express');
const path = require('path');
const PORT = process.env.PORT || 5000;
//import permit from "./permission"; // middleware for checking if user's role is permitted to make request

// packages for logging into MS accounts
const session = require('express-session');
const flash = require('connect-flash');
require('dotenv').config();
var passport = require('passport');
var OIDCStrategy = require('passport-azure-ad').OIDCStrategy;
var graph = require('./graph');

// errors include
const ApplicationError = require('./errors/ApplicationError');

// package for access control
const AccessControl = require('accesscontrol');
const ac = new AccessControl();
ac.grant('unapproved');
ac.grant('user')
	.createOwn('application')
	.readOwn('application', ['*'])
	.readAny('category', ['name'])
	.createOwn('draft')
	.readOwn('draft', ['*'])
	.updateOwn('draft', ['*'])
	.deleteOwn('draft')
	.readAny('program', ['name']);
ac.grant('cardholder')
	.extend('user')
	.readAny('application', ['*'])
	.updateAny('application', ['purchased']);
ac.grant('director')
	.extend('user')
	.readAny('application', ['*'])
	.updateAny('application', ['status'])
	.deleteAny('application')
	.readAny('category', ['*'])
	.createAny('category')
	.updateAny('category', ['*'])
	.deleteAny('category')
	.readAny('program', ['*'])
	.createAny('program')
	.updateAny('program', ['*'])
	.deleteAny('program')
	.readAny('report', ['*'])
	.readAny('user', ['*'])
	.updateAny('user', ['role'])
	.deleteAny('user');
ac.grant('admin')
	.extend('director')
	.updateAny('application', ['*'])
	.updateAny('user', ['*']);

/** 
  * configures passport
  */
// in-memory storage of logged-in users
var users = {};

// passport calls serializeUser and deserializeUser to manage users
passport.serializeUser(function(user, done) {
  
	// uses the OID property of the user as a key
	users[user.profile.oid] = user;
	done (null, user.profile.oid);
});

passport.deserializeUser(function(id, done) {
	done(null, users[id]);
});

// Configure simple-oauth2
const oauth2 = require('simple-oauth2').create({
	client: {
		id: process.env.OAUTH_APP_ID,
		secret: process.env.OAUTH_APP_PASSWORD
	},
	auth: {
		tokenHost: process.env.OAUTH_AUTHORITY,
		authorizePath: process.env.OAUTH_AUTHORIZE_ENDPOINT,
		tokenPath: process.env.OAUTH_TOKEN_ENDPOINT
	}
});

// callback function called once the sign-in is complete and an access token has been obtained
async function signInComplete(iss, sub, profile, accessToken, refreshToken, params, done) {
	if (!profile.oid) {
		return done(new Error("No OID found in user profile."), null);
	}
	try{
		const user = await graph.getUserDetails(accessToken);
		if (user) {

			// adds properties to profile
			profile['email'] = user.mail ? user.mail : user.userPrincipalName;
		}
	} catch (err) {
		done(err, null);
	}

	// creates a simple-oauth2 token from raw tokens
	let oauthToken = oauth2.accessToken.create(params);

	// saves the profile and tokens in user storage
	users[profile.oid] = {
		profile,
		oauthToken
	};
	return done(null, users[profile.oid]);
}

// configures OIDC strategy
passport.use(new OIDCStrategy({
	identityMetadata: `${process.env.OAUTH_AUTHORITY}${process.env.OAUTH_ID_METADATA}`,
	clientID: process.env.OAUTH_APP_ID,
	responseType: 'code id_token',
	responseMode: 'form_post',
	redirectUrl: process.env.OAUTH_REDIRECT_URI,
	allowHttpForRedirectUrl: true,
	clientSecret: process.env.OAUTH_APP_PASSWORD,
	validateIssuer: false,
	passReqToCallback: false,
	scope: process.env.OAUTH_SCOPES.split(' ')
}, signInComplete));

// gets the routers
var indexRouter = require('./routes/index');
var authRouter = require('./routes/auth');

// initializes app
var app = express();
const server = app.listen(PORT, () => console.log(`Listening on ${ PORT }`));
app.use(compression());
app
	// support json encoded bodies
	.use(bodyParser.json())
	// support encoded bodies
	.use(bodyParser.urlencoded({ extended: true }))
	.use(express.static(path.join(__dirname, 'public')))
	.set('views', path.join(__dirname, 'views'))
	.set('view engine', 'ejs');

// session middleware
app.use(session({
	secret: 'ACS_Budgeting_Database',
	resave: false,
	saveUninitialized: false,
	unset: 'destroy'
}));

// flash middleware
app.use(flash());

// Set up local vars for template layout
app.use(function(req, res, next) {
	
	// Read any flashed errors and save in the response locals
	res.locals.error = req.flash('error_msg');

	// Check for simple error string and convert to layout's expected format
	var errs = req.flash('error');
	for (var i in errs){
		res.locals.error.push({message: 'An error occurred', debug: errs[i]});
	}
	next();
});

// initializes passport
app.use(passport.initialize());
app.use(passport.session());

// sets the approved session variable if it is not already set
app.use(function(req, res, next) {
	if(typeof req.session['role'] == 'undefined') {
		req.session['role'] = 'unapproved';
	}
	next();
});

// sets the authenticated user in the template locals
app.use(function(req, res, next) {
	if(req.user) {
		res.locals.user = req.user.profile;
		res.locals.user.role = req.session['role'];
		console.log('Role: ' + res.locals.user.role);
		res.locals.user.permissions = ac.can(res.locals.user.role);
	}
	next();
});

// set the app to use the router files
app.use('/auth', authRouter);
app.use('/', indexRouter);

// sets default error handler
app.use(function(err, req, res, next) {
	var status = 500;
	if(err instanceof ApplicationError) {
		status = err.status;
	}
	res.status(status);
	res.render('error', {
		error: err
	});
});