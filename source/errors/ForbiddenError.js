// for use when the client requests a page they do not have permission to access
const ApplicationError = require('./ApplicationError'); 
class ForbiddenError extends ApplicationError {
	constructor(message) {
		super(message || 'You do not have the necessary permissions to perform that action. Please request access from an admin or the director.', 403);
		this.name = 'Forbidden';
		this.stack = null;
	}
}
module.exports = ForbiddenError;