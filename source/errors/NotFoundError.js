// for use when the client requests a page that does not exist
const ApplicationError = require('./ApplicationError'); 
class NotFoundError extends ApplicationError {
	constructor(url, message) {
		super(message || (url ? `No resource at "${url}" exists.` : 'The resource you requested does not exist.'), 404);
		this.name = 'Not Found';
		this.stack = null;
		this.url = url || '';
	}
}
module.exports = NotFoundError;