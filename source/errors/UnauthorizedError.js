// for use when the server requires authentication but the client fails to authenticate
const ApplicationError = require('./ApplicationError'); 
class UnauthorizedError extends ApplicationError {
	constructor(message) {
		super(message || 'Your request was rejected due to insufficient information for authentication.', 401);
		this.name = 'Unauthorized';
		this.stack = null;
	}
}
module.exports = UnauthorizedError;