// for use when the client's request cannot be fulfilled for some reason
const ApplicationError = require('./ApplicationError'); 
class BadRequestError extends ApplicationError {
	constructor(message) {
		super(message || 'The server could not complete your request likely due to a malformed request.', 400);
		this.name = 'Bad Request';
		this.stack = null;
	}
}
module.exports = BadRequestError;