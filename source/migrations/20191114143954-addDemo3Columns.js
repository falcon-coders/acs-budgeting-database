'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.resolve().then(() => {
      queryInterface.addColumn(
        'applications',
        'description',
        {
          type: Sequelize.BOOLEAN,
          allowNull: true,
          defaultValue: null
        }
      );
    }).then(() => {
      queryInterface.addColumn(
        'applications',
        'budgeted',
        {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: true
        }
      );
    });
  },

  down: (queryInterface, Sequelize) => {
    return Promise.resolve().then(() => {
      queryInterface.removeColumn(
        'applications',
        'description'
      );
    }).then(() => {
      queryInterface.removeColumn(
        'applications',
        'budgeted'
      );
    });
  }
};
