var express = require('express');
var passport = require('passport');
var router = express.Router();
const ApplicationError = require('../errors/ApplicationError');

// redirects the user to the Microsoft signin prompt
router.get('/signin', (req, res, next) => {
    passport.authenticate('azuread-openidconnect', {
        response: res,
        prompt: 'login',
        failureRedirect: '/auth/failure',
        failureFlash: true
    })(req,res,next);
}, (req, res) => {
    res.redirect('/');
});

// authenticates the user with Microsoft before redirecting them to the home page
router.post('/callback', (req, res, next) => {
    passport.authenticate('azuread-openidconnect', {
        response: res,
        failureRedirect: '/auth/failure',
        failureFlash: true
    })(req,res,next);
}, (req, res) => {
    res.redirect('/register');
});

// destroys the user's session and logs them out before redirecting them to the home page
router.get('/signout', (req, res) => {
    req.session.destroy((err) => {
        req.logout();
        res.redirect('/');
    });
});

// throws an error if the app fails to authenticate the client
router.get('/failure', (req, res, next) => {
    next(new ApplicationError('This application was unable to authenticate your account with Microsoft.'));
});
module.exports = router;