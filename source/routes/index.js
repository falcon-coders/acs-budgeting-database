var express = require('express');
var router = express.Router();

// errors
const NotFoundError = require('../errors/NotFoundError');
const UnauthorizedError = require('../errors/UnauthorizedError');

// controllers
var submissions_controller = require('../controllers/submissionsController');
var drafts_controller = require('../controllers/draftsController');
var reports_controller = require('../controllers/reportsController');
var manage_controller = require('../controllers/manageController')
var users_controller = require('../controllers/usersController');

// homepage
router.get('/', (req, res) => {
	var params = {
		bodySrc: './pages/index',
		title: 'Home',
		heading: 'Welcome to ACS, Ft Gordon\'s Budgeting App!'
	};
    res.render('template', params);
});

// checks if the user is logged in before letting them continue
router.get('/*', (req, res, next) => {
	if(res.locals.user == undefined) res.redirect('/auth/signin');
	else next();
});

// blocks any post requests if user is not logged in
router.post('/*', (req, res, next) => {
	if(res.locals.user == undefined) next(new UnauthorizedError());
	else next();
});

// application submissions
router.get('/submissions', submissions_controller.submissions);
router.post('/submissions', submissions_controller.submissions_edit);
router.get('/submissions/submission/:id', submissions_controller.submission);
router.post('/submissions/submission/:id', submissions_controller.submission_save);

// application drafts
router.get('/drafts', drafts_controller.drafts);
router.get('/drafts/draft/:id', drafts_controller.draft);
router.post('/drafts/draft/:id', drafts_controller.create);
router.get('/drafts/new', drafts_controller.new);
router.post('/drafts/new', drafts_controller.create);

// register
router.get('/register', users_controller.register);

// reports
router.get('/reports', reports_controller.get_all_reports);
router.get('/reports/export', reports_controller.export);

// manage
router.get('/manage', manage_controller.manage);
router.post('/manage', manage_controller.manage_edit);
router.get('/manage/export', manage_controller.export);

// catch all other requests
router.get('*', (req, res, next) => {
	throw new NotFoundError(req.url);
});

// exports the router code to be used by the app
module.exports = router;