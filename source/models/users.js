const Sequelize = require('sequelize');
const sequelize = new Sequelize(process.env.DATABASE_URL);

// serialize model of the applications table
const Model = Sequelize.Model;
class Users extends Model {};
Users.init({
    email: {
		type: Sequelize.STRING,
		allowNull: false
	},
	name: {
		type: Sequelize.STRING,
		allowNull: false
	},
	role: {
		type: Sequelize.STRING,
		allowNull: false
	}
}, {
	createdAt: 'created_at',
	updatedAt: 'updated_at',
	sequelize,
	modelName: 'users'
});

module.exports = Users;