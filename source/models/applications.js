const Sequelize = require('sequelize');
const sequelize = new Sequelize(process.env.DATABASE_URL);

// serialize model of the applications table
const Model = Sequelize.Model;
class Applications extends Model {};
Applications.init({
	requester: {
		type: Sequelize.STRING,
		allowNull: false
	},
	program: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	vendor_name: {
		type: Sequelize.STRING,
		allowNull: false
	},
	vendor_phone: {
		type: Sequelize.INTEGER
	},
	vendor_poc: {
		type: Sequelize.STRING
	},
	item: {
		type: Sequelize.STRING,
		allowNull: false
	},
	category: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	color: {
		type: Sequelize.STRING
	},
	size: {
		type: Sequelize.STRING
	},
	imprint: {
		type: Sequelize.STRING
	},
	quantity: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	unit_price: {
		type: Sequelize.DOUBLE,
		allowNull: false
	},
	setup_charges: {
		type: Sequelize.DOUBLE
	},
	shipping: {
		type: Sequelize.DOUBLE
	},
	event: {
		type: Sequelize.STRING
	},
	status: {
		type: Sequelize.STRING
	},
	purchased: {
		type: Sequelize.BOOLEAN,
		allowNull: false
	}
}, {
	createdAt: 'created_at',
	updatedAt: 'updated_at',
	sequelize,
	modelName: 'applications'
});
module.exports = Applications;