const Sequelize = require('sequelize');
const sequelize = new Sequelize(process.env.DATABASE_URL);

// serialize model of the programs table
const Model = Sequelize.Model;
class Categories extends Model {};
Categories.init({
	name: {
		type: Sequelize.STRING,
		allowNull: false
	}
}, {
	createdAt: 'created_at',
	updatedAt: 'updated_at',
	sequelize,
	modelName: 'categories'
});
module.exports = Categories;