// models
const Applications = require('../models/applications');
const Categories = require('../models/categories');
const Programs = require('../models/programs');
const Users = require('../models/users');

// errors
const ForbiddenError = require('../errors/ForbiddenError');
const BadRequestError = require('../errors/BadRequestError');

// packages
const Excel = require('exceljs');

// sends the app's settings to a cleared user
exports.manage = function(req, res, next) {

    // checks if the user has the proper permission
    const permission1 = res.locals.user.permissions.readAny('program');
    const permission2 = res.locals.user.permissions.readAny('category');
    const permission3 = res.locals.user.permissions.readAny('user');
    if(permission1.granted && permission2.granted && permission3.granted) {

        // reads all categories, programs, & users from the database
        var users,
            programs;
        Users.findAll({
            attributes: ['name', 'email', 'role']
        }).then(usersData => {
            users = usersData;
            return Programs.findAll({ attributes: ['id', 'name'] });
        }).then(programsData => {
            programs = programsData;
            return Categories.findAll({ attributes: ['id', 'name'] });
        }).then(categories => {
            
            // renders the manage page
            res.render('template', {
                bodySrc: './pages/manage',
                head: '<script src="/js/pages/manage.js"></script>',
                title: 'App Management',
                heading: 'App Management',
                users: users,
                programs: programs,
                categories: categories,
                breadcrumbs: [{ page: 'Manage' }]
            });
        });
    } else {
        throw new ForbiddenError();
    }
};

// updates the app's categories, programs, and users tables to reflect any changes made by a director/admin
exports.manage_edit = function(req, res, next) {

    // checks if the user has the proper permission
    const permission1 = res.locals.user.permissions.updateAny('program');
    const permission2 = res.locals.user.permissions.updateAny('category');
    const permission3 = res.locals.user.permissions.updateAny('user');
    if(permission1.granted && permission2.granted && permission3.granted) {
        var unvalidatedPrograms = [],
            validatedPrograms = [],
            programUpdateIDs = req.body.programUpdateIDs,
            programUpdateNames = req.body.programUpdateNames;

        // updates the programs that were edited
        if(req.body.programUpdateIDs.length == req.body.programUpdateNames.length) {
            var updatePromises = programUpdateIDs.map((id, index) => {
                return Programs.update({
                    name: programUpdateNames[index]
                }, {
                    where: {
                        id: id
                    }
                });
            });
            Promise.all(updatePromises).then(() => {
                
                // deletes the programs that were deleted
                var programDeletedIDs = [];
                if(typeof req.body.programDeletedIDs !== 'undefined' && req.body.programDeletedIDs.length > 0) {
                    
                    // ensures that all the data is in an array format
                    if(typeof req.body.programDeletedIDs == 'string') {
                        programDeletedIDs.push(req.body.programDeletedIDs);
                    } else if(Array.isArray(req.body.programDeletedIDs)) {
                        programDeletedIDs = req.body.programDeletedIDs;
                    } else throw new BadRequestError();

                    // runs the delete commands
                    var deletePromises = programDeletedIDs.map(id => {
                        return Programs.destroy({
                            where: {
                                id: id
                            }
                        });
                    });
                    Promise.all(deletePromises).then(() => {
                        return;
                    });
                } else return;
            }).then(() => {
                
                // creates the new programs that were named by the user
                var programCreateNames = [];
                if(typeof req.body.programCreateNames !== 'undefined' && req.body.programCreateNames.length > 0) {

                    // ensures that all the data is in an array format
                    if(typeof req.body.programCreateNames == 'string') {
                        programCreateNames.push(req.body.programCreateNames);
                    } else if(Array.isArray(req.body.programCreateNames)) {
                        programCreateNames = req.body.programCreateNames;
                    } else throw new BadRequestError();

                    // runs the delete commands
                    var createPromises = programCreateNames.map(name => {
                        return Programs.create({
                            name: name
                        });
                    });
                    Promise.all(createPromises).then(() => {
                        return;
                    });
                } else return;
            }).then(() => {
                
                // updates categories
                var categoryUpdateNames = req.body.categoryUpdateNames;
                var categoryUpdateIDs = req.body.categoryUpdateIDs;
                if(typeof categoryUpdateNames !== 'undefined' && categoryUpdateNames.length > 0) {
                    if(typeof categoryUpdateIDs !== 'undefined' && categoryUpdateIDs.length > 0) {
                        if(categoryUpdateIDs.length == categoryUpdateNames.length) {
                            var updateCategoriesPromises = categoryUpdateIDs.map((id, index) => {
                                return Categories.update({
                                    name: categoryUpdateNames[index]
                                }, {
                                    where: {
                                        id: id
                                    }
                                });
                            });
                            Promise.all(updateCategoriesPromises).then(() => {
                                return;
                            });
                        } else throw new BadRequestError();
                    } else throw new BadRequestError();
                } else return;
            }).then(() => {
                
                // deletes the programs that were deleted
                var categoryDeletedIDs = [];
                if(typeof req.body.categoryDeletedIDs !== 'undefined' && req.body.categoryDeletedIDs.length > 0) {
                    
                    // ensures that all the data is in an array format
                    if(typeof req.body.categoryDeletedIDs == 'string') {
                        categoryDeletedIDs.push(req.body.categoryDeletedIDs);
                    } else if(Array.isArray(req.body.categoryDeletedIDs)) {
                        categoryDeletedIDs = req.body.categoryDeletedIDs;
                    } else throw new BadRequestError();

                    // runs the delete commands
                    var deleteCategoryPromises = categoryDeletedIDs.map(id => {
                        return Categories.destroy({
                            where: {
                                id: id
                            }
                        });
                    });
                    Promise.all(deleteCategoryPromises).then(() => {
                        return;
                    });
                } else return;
            }).then(() => {

                // creates the new programs that were named by the user
                var categoryCreateNames = [];
                if(typeof req.body.categoryCreateNames !== 'undefined' && req.body.categoryCreateNames.length > 0) {

                    // ensures that all the data is in an array format
                    if(typeof req.body.categoryCreateNames == 'string') {
                        categoryCreateNames.push(req.body.categoryCreateNames);
                    } else if(Array.isArray(req.body.categoryCreateNames)) {
                        categoryCreateNames = req.body.categoryCreateNames;
                    } else throw new BadRequestError();

                    // runs the delete commands
                    var createCategoriesPromises = categoryCreateNames.map(name => {
                        return Categories.create({
                            name: name
                        });
                    });
                    Promise.all(createCategoriesPromises).then(() => {
                        return;
                    });
                } else return;
            }).then(() => {

                // after all changes have been made, redirects back to the manage page
                res.redirect('/manage')
            }).catch(err => {
                next(err);
            });
        } else throw new BadRequestError();
    } else throw new ForbiddenError();
};

// sends a .xlsx version of the database to the user
exports.export = function(req, res, next) {

    // checks if the user has the proper permission
    const permission1 = res.locals.user.permissions.readAny('application');
    const permission2 = res.locals.user.permissions.readAny('category');
    const permission3 = res.locals.user.permissions.readAny('program');
    const permission4 = res.locals.user.permissions.readAny('user');
    if(permission1.granted && permission2.granted && permission3.granted && permission4.granted) {

        // retrieves all entries for the tables
        var tables = [];
        Applications.findAll().then((apps) => {
            tables.push(apps);
            return Categories.findAll();
        }).then((categories) => {
            tables.push(categories);
            return Programs.findAll();
        }).then((programs) => {
            tables.push(programs);
            return Users.findAll();
        }).then((users) => {
            tables.push(users);
            var workbook = new Excel.Workbook();

            // generates worksheet from applications
            tables.forEach((table, index) => {

                // generates sheet based on current table
                var sheet = workbook.addWorksheet(table[0]._modelOptions.name.plural);
                var columns = [];

                // generates headers for the sheet based on current table properties
                var properties = Object.getOwnPropertyNames(table[0].dataValues);
                properties.forEach(property => {
                    columns.push({
                        header: property.replace('_', ' '),
                        key: property
                    });
                });
                sheet.columns = columns;

                // populates the sheet with data from the current table's rows
                table.forEach((row) => {
                    var rowData = {};
                    properties.forEach((property) => {
                        rowData[property] = row[property];
                    });
                    sheet.addRow(rowData);
                });
                sheet.getRow(1).font = { bold: true };
            });

            // sends the workbook as an Excel spreadsheet to the user
            var currentdate = new Date();
            var datetime = `${currentdate.getDate()}/${currentdate.getMonth() + 1}/${currentdate.getFullYear()}@${currentdate.getHours()}:${currentdate.getMinutes()}:${currentdate.getSeconds()}`;
            res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            res.setHeader('Content-Disposition', 'attachment; filename=Backup ' + datetime + '.xlsx');
            workbook.xlsx.write(res).then(() => {
                res.end();
            });
        }).catch((err) => {
            next(err);
        });
    } else throw new ForbiddenError();
}