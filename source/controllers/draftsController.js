// includes the models that this controller will need
const Applications = require('../models/applications');
const Programs = require('../models/programs');
const Categories = require('../models/categories');
const Users = require('../models/users');

// includes the error types that this controller may use
const ApplicationError = require('../errors/ApplicationError');
const BadRequestError = require('../errors/BadRequestError');
const ForbiddenError = require('../errors/ForbiddenError');

// renders drafts with the list of programs
exports.new = function(req, res, next) {

    // checks if the user has the proper permission
    const permission1 = res.locals.user.permissions.createOwn('application');
    const permission2 = res.locals.user.permissions.createOwn('draft');
    if(permission1.granted && permission2.granted) {

        // reads all programs from the database
        Programs.findAll({
        	attributes: ['id', 'name']
        }).then(programs => {
            Categories.findAll({
                attributes: ['id', 'name']
            }).then(categories => {

            	// renders the new application page
            	res.render('template', {
                    bodySrc: './pages/draft',
                    head: '<script src="/js/pages/draft.js"></script>',
                    title: 'Create Application',
                    heading: 'Create a New Purchase Request Application',
                    programs: programs,
                    categories: categories,
                    breadcrumbs: [{
                        page: 'Drafts',
                        path: '/drafts'
                    }, {
                        page: 'New'
                    }]
                });
            });
        });
    } else {
        throw new ForbiddenError();
    }
};

// handles the process of creating a new application and entering it into the database
exports.create = function(req, res, next) {

	// checks if the user has the proper permission
	const permission1 = res.locals.user.permissions.createOwn('application');
	const permission2 = res.locals.user.permissions.createOwn('draft');
	if(permission1.granted && permission2.granted) {
		var localVariables = {};

		// sanitizes & validates input
		var err = null;
        var status;
        if(req.body.action === 'save')
            status = 'draft';
        else if(req.body.action === 'submit')
            status = 'pending';
        else
            throw new BadRequestError();

		// checks if this is a draft or not
		if(typeof req.params.id !== 'undefined' && req.params.id.length > 0) {
			Applications.findOne({
				where: {
					id: req.params.id,
                    status: 'draft'
				}
			}).then(draft => {
				if(draft == null) {
                    throw new BadRequestError();
                } else {
                	Applications.update({
                		requester: res.locals.user.email,
	                    program: req.body.program,
	                    vendor_name: req.body.vendor_name,
	                    item: req.body.itemName,
	                    color: req.body.color,
	                    size: req.body.size,
	                    category: req.body.category,
	                    quantity: req.body.unitQty,
	                    unit_price: req.body.unitCost,
	                    setup_charges: req.body.setup_fee,
	                    shipping: req.body.shipping_fee,
                        status: status,
                        purchased: false
                	}, {
                		where: {
                			id: req.params.id
                		}
                	}).then(([affectedCount, affectedRows]) => {
						res.redirect('/drafts');
					});
                }
			});
		} else {
			Applications.create({
                requester: res.locals.user.email,
                program: req.body.program,
                vendor_name: req.body.vendor_name,
                item: req.body.itemName,
                color: req.body.color,
                size: req.body.size,
                category: req.body.category,
                quantity: (String(req.body.unitQty.length) > 0 ? req.body.unitQty : 0),
                unit_price: req.body.unitCost,
                setup_charges: req.body.setup_fee,
                shipping: req.body.shipping_fee,
                status: status,
                purchased: false
            }).then(application => {
                
                // redirect them to the submitted page if successful
                res.redirect('/drafts');
            });
		}
	} else {
		throw new ForbiddenError();
	}
};

// handles saving/submitting of drafts
exports.edit = function(req, res) {
    
	// checks if the user has the proper permission
    const permission = res.locals.user.permissions.createOwn('draft');
    if(permission.granted) {
        if(req.body.action === 'save') {
            Drafts.create({
                requester: res.locals.user.displayName,
                program: req.body.program,
                vendor_name: req.body.vendor_name,
                item: req.body.itemName,
                color: req.body.color,
                size: req.body.size,
                category: req.body.category,
                quantity: req.body.unitQty,
                unit_price: req.body.unitCost,
                setup_charges: req.body.setup_fee,
                shipping: req.body.shipping_fee
            }).then(application => {
                
                // redirect them to the submitted page if successful
                res.redirect('/drafts');
            });
        }
    } else {
        throw new ForbiddenError();
    }
};

// handles the process of creating a new application and entering it into the database
exports.drafts = function(req, res) {
    
	// checks if the user has the proper permission
    const permission = res.locals.user.permissions.readOwn('draft');
    if(permission.granted) {

        // gets all of the user's drafts
        Applications.findAll({
            attributes: ['id', 'item', 'category', 'unit_price', 'quantity', 'setup_charges', 'shipping', 'created_at'],
            where: {
                requester: res.locals.user.email,
                status: 'draft'
            }
        }).then(drafts => {
            Categories.findAll({
                attributes: ['id', 'name']
            }).then(categories => {

                // renders the new application page
                res.render('template', {
                    bodySrc: './pages/drafts',
                    title: 'Drafts',
                    heading: 'Purchase Request Application Drafts',
                    categories: categories,
                    drafts: drafts,
                    breadcrumbs: [{
                        page: 'Drafts'
                    }]
                });
            });
        });
    } else {
        throw new ForbiddenError();
    }
};

// sends an individual draft to the user
exports.draft = function(req, res) {
    
	// checks if the user has the proper permission
    const permission1 = res.locals.user.permissions.createOwn('application');
    const permission2 = res.locals.user.permissions.updateOwn('draft');
    const permission3 = res.locals.user.permissions.deleteOwn('draft');
    if(permission1.granted && permission2.granted && permission3.granted) {

        // checks that the ID specified is valid
        if(typeof req.params.id !== 'undefined' && req.params.id.length > 0 && !isNaN(req.params.id)) {

            // reads all programs from the database
            Categories.findAll({
                attributes: ['id', 'name']
            }).then(categories => {

                // reads all programs from the database
                Programs.findAll({
                	attributes: ['id', 'name']
                }).then(programs => {

                	// renders the new application page
                    Applications.findOne({
                        where: {
                            id: req.params.id,
                            status: 'draft'
                        }
                    }).then(draft => {
                        res.render('template', {
                            bodySrc: './pages/draft',
                            head: '<script src="/js/pages/draft.js"></script>',
                            title: 'Edit Draft',
                            heading: 'Edit a Purchase Request Draft',
                            draft: draft,
                            programs: programs,
                            categories: categories,
                            breadcrumbs: [{
                                page: 'Drafts',
                                path: '/drafts'
                            }, {
                                page: 'Draft'
                            }]
                        });
                    });
                });
            });
        } else throw new BadRequestError();
    } else {
        throw new ForbiddenError();
    }
};