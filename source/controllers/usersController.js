const Users = require('../models/users');
const UnauthorizedError = require('../errors/UnauthorizedError');

// registers users by adding them to the Users table
exports.register = function(req, res, next) {

	// checks that the user has a first and last name before letting them register
	if(res.locals.user.displayName !== undefined && res.locals.user.displayName !== null && res.locals.user.displayName.trim().length > 0) {

		// registers the user if they are new and/or retrieves the user's role
		Users.findOrCreate({
			where: {
				email: res.locals.user.email
			}, 
			defaults: {
				name: res.locals.user.displayName,
				role: 'unapproved'
			}
		}).then(([user, created]) => {
			console.log('Created New User? ' + created);
			req.session['role'] = user.role;
			res.redirect('/');
		});
	} else {

		// logs the user out and throws an error
		req.session.destroy(function(err) {
            req.logout();
            res.locals.user = undefined;
            next(new UnauthorizedError('A first name and last name is required in order to register as a user on this site. You have been logged out. Please edit your name in your Microsoft Live account before attempting to re-register.'));
        });
	}
}