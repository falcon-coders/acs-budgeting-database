const Sequelize = require('sequelize');

// includes the models that this controller will need
const Applications = require('../models/applications');
const Programs = require('../models/programs');
const Categories = require('../models/categories');
const Users = require('../models/users');

// includes the error types that this controller may use
const ApplicationError = require('../errors/ApplicationError');
const BadRequestError = require('../errors/BadRequestError');
const ForbiddenError = require('../errors/ForbiddenError');

// creates default text variables for certain pages
var submittedHeading = 'Submitted Purchase Request Applications';
var submittedTitle = 'Submitted Applications';
var applicationHeading = 'Submitted Purchase Request Application';
var applicationTitle = 'Application';

// shows the user a list of all their submitted applications with their status
exports.submissions = function(req, res) {
	
	// checks if the user has the proper permission
	var permissions = res.locals.user.permissions;
	const isDirector = permissions.readAny('application').granted && (permissions.updateAny('application').attributes.includes('*') || permissions.updateAny('application').attributes.includes('status'));
	const isCardholder = permissions.readAny('application').granted && permissions.updateAny('application').attributes.includes('purchased');
	const isUser = permissions.readOwn('application').granted;
	var where = {};
	const Op = Sequelize.Op;
	if(isDirector) {
		where = {
			status: {
				[Op.ne]: 'draft'
			}
		}
	} else if(isCardholder) {
		where = {
			purchased: false
		};
	} else if(isUser) {
		where = {
			requester: res.locals.user.email,
			status: {
				[Op.ne]: 'draft'
			}
		};
	} else {
		throw new ForbiddenError();
	}

	// reads rows from the table and sends them to the page
	Applications.findAll({
    	attributes: ['status', 'category', 'id', 'item', 'program', 'quantity', 'requester', 'setup_charges', 'shipping', 'unit_price', 'purchased', 'created_at'],
    	where: where
    }).then(applications => {
		Programs.findAll({
			attributes: ['id', 'name']
		}).then(programs => {
			Users.findAll({
    			attributes: ['email', 'name']
    		}).then(users => {

    			// sends the submitted page back to the user with the data
				res.render('template', {
					bodySrc: './pages/submissions',
					head: '<link href="/css/pages/submissions.css" rel="stylesheet" type="text/css" /><script src="/js/pages/submissions.js"></script>',
					title: submittedTitle,
					heading: submittedHeading,
					applications: applications,
					programs: programs,
					users: users,
					breadcrumbs: [{ page: 'Submissions' }]
				});
			});
		});
	});
};

// saves changes made to the status of submitted applications by directors & above
exports.submissions_edit = function(req, res, next) {

	// verifies that the user has the necessary permissions and checks if the user will be updating 'status' or 'purchased' in submissions
	var permission = res.locals.user.permissions.updateAny('application');
	var column;
	if(permission.granted && (permission.attributes.includes('status') || permission.attributes.includes('*'))) {
		column = 'status';
	} else if(permission.granted && (permission.attributes.includes('purchased') || permission.attributes.includes('*'))) {
		column = 'purchased';
	} else {
		throw new ForbiddenError();
	}

	// checks that each ID is a number
	var isValid = true;
	var readPromises;
	var submissionIDs = Object.keys(req.body);
	submissionIDs.forEach((id) => {
		if(isNaN(id)) next(new BadRequestError());
	});

	// checks that each ID is in the DB
	Applications.findAll({
		attributes: [ column ],
		where: {
			id: submissionIDs
		}
	}).then(applications => {
		if(applications.length == submissionIDs.length) {

			// updates all submissions
			var updatePromises = submissionIDs.map(id => {
				var columns = {};
				columns[column] = req.body[id];
				return Applications.update(columns, {
					where: {
						id: id
					}
				});
			});
			Promise.all(updatePromises).then(() => {
				res.redirect('/submissions');
			}).catch(err => {
				next(err);
			});
		} else next(new BadRequestError());
	});
}

// shows the user an individual application
exports.submission = function(req, res) {
	
	// checks if the user has the proper permission
	const permission = res.locals.user.permissions.readAny('application');
	if(permission.granted) {
		if(typeof req.params.id !== 'undefined' && req.params.id.length > 0 && !isNaN(req.params.id)) {
			//var id = parseInt(req.query.id);
			Applications.findOne({
				where: {
					id: req.params.id
				}
			}).then(application => {
				Categories.findAll({
		    		attributes: ['id', 'name']
		    	}).then(categories => {
		    		Programs.findAll({
		    			attributes: ['id', 'name']
		    		}).then(programs => {
		    			Users.findAll({
			    			attributes: ['email', 'name']
			    		}).then(users => {
							res.render('template', {
								bodySrc: './pages/submission',
								head: '<script src="/js/pages/submission.js"></script>',
								title: application.item,
								heading: 'Purchase Request Application',
								application: application,
								programs: programs,
								categories: categories,
								users: users,
								breadcrumbs: [{
									page: 'Submissions',
									path: '/submissions'
								}, {
									page: 'Submission'
								}]
							});
						});
					});
				});
			});
		} else {
			throw new BadRequestError();
		}
	} else {
		throw new ForbiddenError();
	}
};

exports.submission_save = function(req, res, next) {
	if(req.body.action == 'delete') {
		Applications.destroy({
			where: {
				id: req.params.id
			}
		}).then(() => {
			res.redirect('/submissions');
		}).catch(err => {
			next(err);
		});
	} else if(req.body.action == 'approved' || req.body.action == 'wishlist' || req.body.action == 'denied') {
		Applications.update({
			status: req.body.action
		}, {
			where: {
				id: req.params.id
			}
		}).then(() => {
			res.redirect('/submissions');
		}).catch(err => {
			next(err);
		});
	} else {
		next(new Error(`'${req.body.action}' is not an action.`));
	}
};