// models
const Applications = require('../models/applications');
const Programs = require('../models/programs');
const Categories = require('../models/categories');

// errors
const ForbiddenError = require('../errors/ForbiddenError');

// packages
const PDFDocument = require('pdfkit');

// sends all data for reports to the user
exports.get_all_reports = function(req, res, next) {

	// checks if the user has the proper permission
    const permission = res.locals.user.permissions.readAny('application');
    if(permission.granted) {
		Programs.findAll({
            attributes: [ 'id', 'name' ]
        }).then(programs => {
            Categories.findAll({
                attributes: [ 'id', 'name' ]
            }).then(categories => {
                Applications.findAll({
                	attributes: [ 'status', 'purchased', 'category', 'program', 'quantity', 'requester', 'setup_charges', 'shipping', 'unit_price' ],
                    where: {
                        status: 'approved'
                    }
                }).then(applications => {

                	// defines additional files/JS that the reports will need
                	var head = `
                	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
                	<script>
                        var applications = ${JSON.stringify(applications)};
                        var programs = ${JSON.stringify(programs)};
                        var categories = ${JSON.stringify(categories)};
                        console.log('Applications:');
                        console.log(applications);
                        console.log('Programs:');
                        console.log(programs);
                        console.log('Categories:');
                        console.log(categories);
                	</script>
                	<script src="/js/pages/reports.js"></script>
                	`;

                	// renders the reports page with the necessary data and frontend JS files
                	res.render('template', {
        				bodySrc: './pages/reports',
        				head: head,
        				title: 'Reports',
        				heading: 'Reports',
                        programs: programs,
                        breadcrumbs: [{ page: 'Reports' }]
        			});
                }).catch(err => {
                    next(err);
                });
            }).catch(err => {
                next(err);
            });
        }).catch(err => {
            next(err);
        });
	} else {
        throw new ForbiddenError();
    }
};

exports.export = function(req, res, next) {

    // creates a PDF document
    var currentdate = new Date();
    var datetime = `${currentdate.getDate()}/${currentdate.getMonth() + 1}/${currentdate.getFullYear()}@${currentdate.getHours()}:${currentdate.getMinutes()}:${currentdate.getSeconds()}`;
    const doc = new PDFDocument();

    // populates the PDF with content
    var title = 'Number of Purchases for all Programs';
    doc.fontSize(25).text(title, { align: 'center' });
    doc.fontSize(18).text('From Q1 2019 to Q1 2020', { align: 'center' });
    doc.fontSize(12).text('Created by ' + res.locals.user.displayName + ' on ' + datetime, { align: 'center' });
    doc.translate(300, 300)
        .circle(0, 0, 100).lineWidth(1).fillOpacity(0.35).fillAndStroke('#FF6384', '#FF6384');
    doc.end();
     
    // send PDF to the user
    res.setHeader('Content-Type', 'application/pdf');
    res.setHeader('Content-Disposition', 'attachment; filename=Report ' + datetime + '.pdf');
    doc.pipe(res);
    res.end();
};