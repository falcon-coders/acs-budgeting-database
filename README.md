# ACS Budgeting Database
A Web app for tracking applications for official purchase requests and for analyzing purchase data within the Army Community Service office on Fort Gordon.
## Development:
The app is written primarily in JavaScript which runs in the Node.js runtime environment.  It uses the Express.js framework to create the backend for the Web server. The app's source code is stored in a private repository on GitLab. This repository is hooked into a Heroku pipeline. On Heroku, we have a staging app and a production app. The staging app is where development and testing is done. This version is NOT suitable for general use. The production app is for general use. This is the official version that will be used by the customer.

Each of our Heroku apps runs in a separate environment on Amazon Web Services (AWS), which is managed nearly entirely by Heroku, and are connected to their own PostgreSQL database instances. Therefore, there is no danger of data in the production server being corrupted by testing and development.
## Design:
The app is designed using the model-view-controller (MVC) architecture. Models provide an abstract way to represent data in the database. Views provide data for the user to see and interact with. Controllers act as the middleman between the models and views. They contain the main logic of the app and control how the user can interact with the data.

When a user makes a request to the server, the data runs through our middleware, which handles things like user accounts and sessions, in the main Express file (`./source/index.js`) before the request is routed to one of the router files. The routers then forward the request to the proper controller based on what resource the user requested, what action they are trying to perform, etc. From there, the controllers determine what to do with the data requested or provided, interact with various models to retrieve or modify data, then send any results or messages to one of the views, which is ultimately what the user sees.
### Models
The models are all found in the `./source/models` directory. Each model is a JS object that represents a certain table in our database. The process of creating these models is called Object-Relational Mapping (ORM). This process is largely handled by a package we are using called Sequelize, which we interact with to define the structure of the table we are trying to represent. As Sequelize objects, our models have numerous methods which we use to interact with the table in question without having to write any SQL code at all.
### Controllers
The controllers are all found in the `./source/controllers` directory. The app's controllers are where the bulk of the program's logic lies. When the user requests different URLs, a router outsources most of those requests to one of the controllers. There is a controller for each of the program's major features. These are:

 - Applications: handles all interactions that involve the editing, submitting, and saving of purchase request applications.
 - Reports: handles all interactions that involve the creation of graphics that are designed to give the Director insight into how, where, and why funds are being spent.
 - Settings: handles all interactions that involve the alteration of various pieces of data that are used across the app, such as user permissions, purchase categories, etc.

Once the request has been routed to a controller, the controller then checks the user's permissions, sanitizes data, interacts with the models to perform CRUD operations, etc, then forwards any messages or data to be displayed to the user to one of the views.
### Views
The views are all found in the `./source/views` directory. Each view receives data from the controller that rendered it and uses that data to generate HTML that will be sent to the user. Our app uses the EJS package as its view engine. This was chosen due to its popularity and flexibility. EJS allows us to create templates and embed backend JS code directly into an HTML file. This backend code gets executed before the server sends the HTML to the user. This is how the views generate HTML that will differ from user to user.